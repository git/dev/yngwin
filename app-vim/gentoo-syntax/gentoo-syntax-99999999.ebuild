# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4

inherit git-2 vim-plugin

DESCRIPTION="Vim syntax highlighting, filetype and indent settings for Gentoo filetypes"
HOMEPAGE="http://www.gentoo.org/"
SRC_URI=""
EGIT_REPO_URI="git://git.overlays.gentoo.org/proj/gentoo-syntax.git
	http://git.overlays.gentoo.org/proj/gentoo-syntax.git"

LICENSE="vim"
KEYWORDS=""
IUSE="ignore-glep31"

VIM_PLUGIN_HELPFILES="gentoo-syntax"
VIM_PLUGIN_MESSAGES="filetype"

src_prepare() {
	if use ignore-glep31 ; then
		for f in ftplugin/*.vim ; do
			ebegin "Removing UTF-8 rules from ${f} ..."
			sed -i -e 's~\(setlocal fileencoding=utf-8\)~" \1~' ${f} \
				|| die "waah! bad sed voodoo. need more goats."
			eend $?
		done
	fi
	rm -r .git Makefile || die
}

pkg_postinst() {
	vim-plugin_pkg_postinst
	if use ignore-glep31 1>/dev/null ; then
		ewarn "You have chosen to disable the rules which ensure GLEP 31"
		ewarn "compliance. When editing ebuilds, please make sure you get"
		ewarn "the character set correct."
	fi
}
