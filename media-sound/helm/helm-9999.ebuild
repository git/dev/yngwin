# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=5
inherit eutils

DESCRIPTION="Open source polyphonic software synthesizer with lots of modulation"
HOMEPAGE="http://tytel.org/helm/"

LICENSE="GPL-3+ CC-BY-3.0"
SLOT="0"
IUSE="vst"

if [[ ${PV} == *9999* ]]; then
	EGIT_REPO_URI="https://github.com/mtytel/helm.git"
	inherit git-r3
else
	SRC_URI="https://github.com/mtytel/helm/archive/v${PV}.tar.gz -> ${P}.tar.gz"
	KEYWORDS="~amd64"
fi

RDEPEND="media-libs/alsa-lib
	media-libs/lv2
	media-sound/jack-audio-connection-kit
	virtual/opengl
	x11-libs/libX11
	x11-libs/libXext"
DEPEND="${RDEPEND}
	vst? ( >=media-libs/vst-sdk-2.4 )"

DOCS="README.md"

src_prepare() {
	rm patches/Synth/.DS_Store || die
	rm {images,patches}/LICENSE || die
	epatch "${FILESDIR}"/helm-vst-location.patch
	sed -e 's|/usr/lib/|/usr/'$(get_libdir)'/|' -i Makefile || die
	epatch_user
}

src_compile() {
	emake PREFIX=/usr all $(usex vst vst '')
}

src_install() {
	default
	if use vst; then
		exeinto /usr/$(get_libdir)/vst
		doexe builds/linux/VST/build/helm.so
	fi
	make_desktop_entry /usr/bin/helm Helm /usr/share/helm/icons/helm_icon_32_1x.png
}
