# Copyright 1999-2015 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=5
inherit versionator

BDATE="06_08_2015"
BUILD="build_50"
MY_PV="$(delete_all_version_separators)_${BDATE}_${BUILD}"

DESCRIPTION="Steinberg Virtual Studio Technology software development kit"
HOMEPAGE="http://www.steinberg.net/en/company/developers.html"
SRC_URI="http://www.steinberg.net/sdk_downloads/vstsdk${MY_PV}.zip"

LICENSE="VST3"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

DEPEND="app-arch/unzip"
RDEPEND=""

RESTRICT="mirror"
S=${WORKDIR}

src_prepare() {
	mv "VST3 SDK" "vst3-sdk" || die
}

src_compile() { :; }

src_install() {
	insinto /usr/share
	doins -r vst3-sdk
}

pkg_postinst() {
	einfo "Please make sure to review the license agreement, which can be found at:"
	einfo "${ROOT}usr/share/vst3-sdk/doc/VST3_License_Agreement.html"
}
